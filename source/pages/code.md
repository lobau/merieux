---
title: "Test Code"
author: "Laurent Baumann"
---
Test of HTML Code

```HTML
<mr-app>
    <mr-surface>
    <mr-container>
        <mr-panel></mr-panel>
        <mr-row>
            <mr-text>
                This is a quick example of an image gallery with explainer text.
            </mr-text>
            <mr-column>
                <mr-image src="..."></mr-image>
                <mr-row height="0.02">
                    <mr-button onClick="Prev()"> <- </mr-button>
                    <mr-button onClick="Next()"> -> </mr-button>
                </mr-row>
            </mr-column>
        </mr-row>
    </mr-container>
    </mr-surface>
</mr-app>
```